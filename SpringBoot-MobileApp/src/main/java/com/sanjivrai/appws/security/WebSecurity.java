package com.sanjivrai.appws.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.sanjivrai.appws.service.UserService;

//WebSecurityConfigurerAdapter already present in the spring security boot project 
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{

private final UserService userDetailsService; //custom implementaion of UserDetailsSerive od spring security module

private final BCryptPasswordEncoder bCryptPasswordEncoder;



public WebSecurity(UserService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
	this.userDetailsService = userDetailsService;
	this.bCryptPasswordEncoder = bCryptPasswordEncoder;
}



@Override
protected void configure(HttpSecurity http) throws Exception
{
	  http.csrf().disable().authorizeRequests()
	  .antMatchers(HttpMethod.POST,"/users")
	  .permitAll() /*it allows /user to use publically without authentication for signup process */
	  .anyRequest().authenticated(); /*the rest service neeed to be authenticated properly*/
}
   
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
}


//public void cofigure(AuthenticationManagerBuilder auth) throws Exception
//	{
//		auth.userDetailsService(userDetailService).passwordEncoder(bCryptPasswordEncoder);
//	}
//
}
