package com.sanjivrai.appws.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sanjivrai.appws.UserRepository;
import com.sanjivrai.appws.io.entity.UserEntity;
import com.sanjivrai.appws.service.UserService;
import com.sanjivrai.appws.shared.dto.UserDto;
import com.sanjivrai.appws.utility.Utils;

@Service
public class UserServiceImpl implements UserService {
    
	@Autowired
	UserRepository	userRepository;
	
	@Autowired
	BCryptPasswordEncoder bcpd;
	
	@Autowired
	Utils utils;
	
	@Override
	public UserDto createUser(UserDto user)
	{
	
	UserEntity userentity=new UserEntity();	
	BeanUtils.copyProperties(user, userentity);	
	System.out.println("encrypted password"+bcpd.encode(user.getPassword()));
	String password=bcpd.encode(user.getPassword());
	userentity.setEncryptedPassword(password);
	String userid=utils.userId();
	userentity.setUserId(userid);
	System.out.println("email to be searched "+user.getEmail());
	System.out.println(userRepository.findByEmail(user.getEmail()));
	if(userRepository.findByEmail(user.getEmail()) !=  null) throw new RuntimeException("email already there");
	
	UserEntity storedEntity=userRepository.save(userentity);
	
	UserDto returnValue=new UserDto();
	
	BeanUtils.copyProperties(storedEntity,returnValue);
	
	return returnValue;
	}

	@Override  //overrides the UserDetailsService class method which is exteted by UserService interface for giving the custom Implemetnation to UserDetailService class of Spring.Security
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

}
