package com.sanjivrai.appws.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.sanjivrai.appws.shared.dto.UserDto;

public interface UserService extends UserDetailsService{

UserDto createUser(UserDto user);	

}
