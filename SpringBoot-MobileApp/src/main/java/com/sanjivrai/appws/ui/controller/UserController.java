package com.sanjivrai.appws.ui.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanjivrai.appws.service.UserService;
import com.sanjivrai.appws.shared.dto.UserDto;
import com.sanjivrai.appws.ui.model.request.UserDetailsRequestModel;
import com.sanjivrai.appws.ui.model.response.UserRest;

@RestController
@RequestMapping("users")
public class UserController {

@Autowired	
UserService userService;	
	
@GetMapping	
public String getUser()
{
	return "get user was called";
}
	
@PostMapping	
public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails)
{
	//response
	UserRest returnValue=new UserRest();
	
	UserDto userdto=new UserDto();
	
	BeanUtils.copyProperties(userDetails,userdto);
	UserDto createUser=userService.createUser(userdto);
	
	BeanUtils.copyProperties(createUser,returnValue);
	
	return returnValue;
}


@DeleteMapping	
public String deleteUser()
{
	return "Delete user was called";
}
	
@PutMapping
public String updateUser()
{
	return "Updated user";
}

}
