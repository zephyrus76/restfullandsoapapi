package com.sanjivrai.appws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SpringBootMobileAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMobileAppApplication.class, args);
		
	}

	//for encrypting user password
	
	@Bean	
	public BCryptPasswordEncoder bcryptpassword()
	{
		return new BCryptPasswordEncoder();
	}	
	
}
